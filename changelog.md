version 0.9.9 (August 1, 2013)
- lazyload update
- screenshot preview on filter page
- branded login page
- timthumb update
- unified design
- removed unnecessary css and code
- many fixes and improvements
- apple touch icons
- web app template

version 0.9.7.1 (May 12, 2012)
- separate author page
- category page fix to fit the width correctly

version 0.9.7.0 (May 04, 2012)
- css text display fixes
- support for Disquss, WP-PageViews
- about author
- timthumb update

version 0.9.6.2 (February 25, 2012)
- [img] shortcode to use TimThumb inside posts.
- transparency for header and footer added to work great with a custom background
- css3 improvements
- unused resorces removed
- started the _s theme adaptation

version 0.9.6.0 (February 6, 2012)
- native wordpress menu enabled
- 4 filters types
- better transparent logo and new favicon
- lots of css3 and htlm5 fixes

version 0.9.5.0 (February 1, 2012)
- post thumbnails
- backgorund image enabled
- add the the items filter
- text readability improved with Google fonts
- 4 widgets footer
- many css3 and htlm5 improvements

version 0.9.0.7 (March 29, 2011)
- text readability improved
- lots of HTML and CSS fixes
- sidebar widgetized 

version 0.8.5.1 (March 17, 2011)
- widgetize footer area
- css3 shadow features added
- html5 enabled
- lots of fixes

version 0.8.4.0 (November 12, 2010)
- lots of fixes

version 0.8.0.0 (October 12, 2010)
- ported to WordPress
- many css and php fixes.
- first version of Oceania css theme