<?php
/*
Template Name: Sitemap
*/
?>
<?php get_header(); ?>

<div id="page">

	<h1><?php the_title(); ?></h1>

	<div class="page_column">
		<h2>All Articles</h2>
		<ul class="page_column_list">
			<?php global $post;
			$myposts = get_posts('numberposts=-1&offset=1');
			foreach($myposts as $post) :
			?>
			<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
			<?php endforeach; ?>
		</ul>
	</div>

	<div class="page_column">
		<h2>Articles by Category</h2>
		<ul class="page_column_list">
			<?php wp_list_categories('show_count=0&title_li=&depth=1&exclude=1');?>
		</ul>
		
		<h2>Articles by Month</h2>
		<ul class="page_column_list">
			<?php wp_get_archives('type=monthly'); ?>
		</ul>
	</div>

	<div class="page_column">
		<h2>Articles by Tags</h2>
			<?php wp_tag_cloud('smallest=8&largest=16&number=500'); ?>
	</div>	

</div>

<?php get_footer(); ?>