<?php
/*
Template Name: Members
*/
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
<title><?php global $page, $paged; wp_title(''); if(wp_title('', false)) { echo ' - '; } bloginfo('name'); if(is_home()) { echo ' - '; bloginfo('description'); } if ( $paged >= 2 || $page >= 2 ) echo ' | ' . sprintf( __( 'Page %s', 'oceania'), max( $paged, $page ) );?></title>

<!-- Webmasters Meta Code -->
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" >
<meta name="google-site-verification" content="9KHrUJmN6YBwFOsjTMw15Iya5oFyQxPYjRp70JcyWzc" >
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri() ?>/img/favicon.ico" />

<!-- WordPress Code  -->
<?php wp_head(); ?>
</head>

<!-- Body Content-->
<body <?php echo body_class();?>>

<div id="container">

	<div id="header">

		<div id="rcp_header">
	
			<div id="logo">
		
				<a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/kreativsound.png" alt="<?php bloginfo('name'); ?>" width="50" height="50"/></a>
			
			</div>
		
			<div id="title">
		
				<div id="name">
					
					<a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?> ~ <?php bloginfo('description'); ?>"><?php bloginfo('name'); ?></a>
				
				</div>
			
				<div id="description">
					
					<?php bloginfo('description'); ?>
				
				</div>
			
			</div>
		
		</div>
		
	</div>

	<div id="rcp_members" class="center">
	
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<h1><?php the_title(); ?></h1>
			
			<?php the_content('Read the rest of this page &larr;'); ?>
			<?php wp_link_pages(array('before' => '<strong>Pages:</strong> ', 'after' => '', 'next_or_number' => 'number')); ?>
					
		<?php endwhile; endif; ?>
		
	</div>
	
	<div id="footer_static">

		<!-- Edit the line below with your website info -->
		<a href="<?php echo home_url(); ?>/about">About <?php bloginfo('name'); ?></a> &copy;2012 | <a href="<?php echo home_url(); ?>/contact">Contact</a>, <a href="<?php bloginfo('rss2_url'); ?>">Articles (RSS)</a> and <a href="<?php bloginfo('comments_rss2_url'); ?>">Comments (RSS)</a>. <a href="<?php echo home_url(); ?>/popular" title="Popular">Popular</a> | <a href="<?php echo home_url(); ?>/sitemap" title="Sitemap">Sitemap</a> 
		<br/><br/>

		<!-- Please support WordPress and Oceania by not removing the "Powered by" links. Thank you! -->
		Powered by <a href="http://wordpress.org/">WordPress</a> and <a href="http://www.kreativtheme.com/oceania">Oceania</a>
		<br/><br/>
		
		<!-- Certified Domain Site Seal by GoDaddy! -->
		<div align="center"><span id="cdSiteSeal1"><script type="text/javascript" src="//tracedseals.starfieldtech.com/siteseal/get?scriptId=cdSiteSeal1&amp;cdSealType=Seal1&amp;sealId=55e4ye7y7mb73f5e7e3a83cdad9110nrcby7mb7355e4ye7b5414ec6795e518b9"></script></span></div>
		
		<!-- Do not delete this -->
		<?php wp_footer(); ?>

	</div>	

</div>

</body>

</html>