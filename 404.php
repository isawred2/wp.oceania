<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Oceania
 * @since Oceania 1.0
 */

get_header(); ?>

<div id="page">

	<div class="center">
		
		<h2>Nothing found</h2>
		
		<p>Hi! We are sorry, but the article you are looking for does not exist or has been moved!
		<br/>
		
		You can <b>use the search field</b> to find it or you can <a href="<?php home_url(); ?>"><b>go to home page</b></a>.
		<br/>
		
		Thank you!</p>

	</div>
	
</div>

<?php get_footer(); ?>
