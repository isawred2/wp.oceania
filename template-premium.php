<?php
/*
Template Name: Filter Premium
*/
?>
<?php get_header(); ?>

<div id="page">

<div class="center">You are a Kreativ Sound PREMIUM member. <strong>THANK YOU</strong>!<br/>
You are entitled to download and use in you productions the entire sounds library below!

<img src="http://www.kreativsounds.com/uploads/2012/03/Kreativ-Sound-Premium-Crown.png" alt="Kreativ Sound PREMIUM Account Crown" title="Kreativ Sound PREMIUM Account Crown" width="150" height="80" class="aligncenter size-full wp-image-3091" /></div>

	<ul id="portfolio">	
	
	<?php query_posts( 'cat=7&posts_per_page=50');

		if ( have_posts() ) : while ( have_posts() ) : the_post();?>

		<li class="reset <?php $terms = get_the_terms( $post->id, 'category'); if ($terms) foreach( $terms as $term ) { print $term->slug . ' '; unset($term); }; $tags = wp_get_post_tags($post->ID); if ($tags) foreach($tags as $tag) { print $tag->slug . ' '; unset($tag); }?>">
			
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">				 
				
			<?php the_title(); ?>
				
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
			
			<img src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php echo $image[0]; ?>&amp;h=100&amp;w=160&amp;zc=1&amp;a=t" alt="<?php the_title(); ?>" height="100" width="160" />
							
			</a>
				
			<div class="cart">
				
				<a href="<?php echo home_url(); ?>/get/<?php print $slug = basename(get_permalink()); ?>" target="_blank" title="Download <?php the_title(); ?>">Download Sounds &darr;</a>
			
			</div>
			
		</li>

		<?php endwhile; endif; ?>
	
	<?php wp_reset_query(); ?>
		
    </ul>
	
</div>

<?php get_footer(); ?>