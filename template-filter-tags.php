<?php
/*
Template Name: Filter Tags

With this template you can choose 3 filter types for the items: Ajaxified Menu and Tags
*/
?>
<?php get_header(); ?>

<!-- Filter Javascript -->
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/js/filter-tags.js"></script>

<div id="page">

	<div id="filter-all">
	
		Filter
		
		<ul id="filter">
		
			<li><a href="#sounds">All</a></li>
		
		</ul>
		
	</div>

	<div id="filter-sound">
	
		Sounds
		
		<ul id="filter">
				
			<li><a href="#audio-loops">Audio Loops</a></li>
			
			<li><a href="#multisamples">Multisamples</a></li>
			
			<li><a href="#reason-refills">Reason Refills</a></li>
			
			<li><a href="#vst-presets">VST Presets</a></li>
						
		</ul>
		
	</div>	
	
	<div id="filter-soft">
	
		Software
		
		<ul id="filter">
		
			<li><a href="#bass-station">Bass Station</a></li>
			<li><a href="#combinator">Combinator</a></li>
			<li><a href="#dr-octorex">Dr Octorex</a></li>
			<li><a href="#filterscape">Filterscape</a></li>
			<li><a href="#fm8">FM8</a></li>
			<li><a href="#free">FREE</a></li>
			<li><a href="#gladiator">Gladiator</a></li>
			<li><a href="#imposcar">ImpOSCar</a></li>
			<li><a href="#kontakt">Kontakt</a></li>
			<li><a href="#kore">KORE</a></li>
			<li><a href="#malstrom">Malstrom</a></li>
			<li><a href="#minimoog-v">Minimoog V</a></li>
			<li><a href="#nn19">NN19</a></li>
			<li><a href="#nnxt">NNXt</a></li>
			<li><a href="#one">ONE</a></li>
			<li><a href="#plasticz">PlastiCZ</a></li>
			<li><a href="#pro-53">PRO-53</a></li>
			<li><a href="#prophet-v">Prophet V</a></li>
			<li><a href="#redrum">ReDrum</a></li>
			<li><a href="#rv7000">RV7000</a></li>
			<li><a href="#scream4">Scream4</a></li>
			<li><a href="#sfz">SFZ</a></li>
			<li><a href="#subtractor">Subtractor</a></li>
			<li><a href="#sylenth1">Sylenth1</a></li>
			<li><a href="#thor">THOR</a></li>
			<li><a href="#vanguard">Vanguard</a></li>
			
		</ul>
		
	</div>
	
	<div id="filter-format">
	
		Formats
		
		<ul id="filter">
			<li><a href="#acid">ACID</a></li>
			<li><a href="#rex">REX</a></li>
			<li><a href="#wav">WAV</a></li>
			<li><a href="#aiff">AIFF</a></li>		
			<li><a href="#16bit">16Bit</a></li>
			<li><a href="#24bit">24Bit</a></li>
			<li><a href="#patch">Patch</a></li>
		</ul>
		
	</div>
	
	<div class="center"><a href="http://www.kreativsounds.com/free-sounds/"><strong>Join Today to get Unlimited Access to all our FREE sounds</strong></a>!</div>
	
	<ul id="portfolio">	
	
	<li class="sounds vst-presets fm8 kore patch abyss kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-abyss-fm8-sounds" title="ABYSS FM8 Sounds">ABYSS FM8 Sounds<br/>
				<img src="http://www.kreativsounds.com/img/presets/abyss-fm8-sounds-small.png" alt="ABYSS FM8 Sounds" class="image" width="100" height="95"/>
			</a>
			<br/>
			32 digital madness sounds for Native Instruments FM8
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS202">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="L2KMQ7NCWKVY2">
				<a href="#add2cart" onClick="javascript:document['KS202'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>
		
		<li class="sounds vst-presets plasticz kore patch abyss kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-abyss-plasticz-sounds" title="ABYSS PlastiCZ Sounds">ABYSS PlastiCZ Sounds<br/>
				<img src="http://www.kreativsounds.com/img/presets/ABYSS-PlastiCZ-Sounds-thumb.png" alt="ABYSS PlastiCZ Sounds" class="image" width="215" height="92"/>
			</a>
			<br/>			
			90 lofi digital emotive sounds for reFX PlastiCZ
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS210">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="2WT838J76FG5E">
				<a href="#add2cart" onClick="javascript:document['KS210'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds vst-presets pro-53 kore patch abyss kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-abyss-pro-53-sounds" title="ABYSS PRO-53 Sounds">ABYSS PRO-53 Sounds<br/>
				<img src="http://www.kreativsounds.com/img/presets/ABYSS-Pro-53-Sounds-thumb.png" alt="ABYSS PRO-53 Sounds" class="image" width="215" height="92"/>
			</a>
			<br/>
			202 prophetic & warm sounds for Native Instruments PRO-53
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS201">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="E9GGLGGDH3ZEE">
				<a href="#add2cart" onClick="javascript:document['KS201'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds vst-presets gladiator patch culture-electronic center ">
			<a href="http://www.kreativsounds.com/ce-gladiator-dance-sounds-volume-1" title="CE Gladiator Dance Sounds Volume 1">CE Gladiator Dance Sounds Vol.1<br/>
				<img src="http://www.kreativsounds.com/img/presets/culture-electronic-gladiator-dance-sounds-v1-small.png" alt="CE Gladiator Dance Sounds Volume 1" class="image" width="100" height="95"/>
			</a>
			<br/>
			180 fantastic dance sounds for Tone2 Gladiator
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="CE001">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="E3Q4TXVFE8VAS">
				<a href="#add2cart" onClick="javascript:document['CE001'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds vst-presets gladiator patch culture-electronic center ">
			<a href="http://www.kreativsounds.com/ce-gladiator-dance-sounds-volume-2" title="CE Gladiator Dance Sounds Volume 2">CE Gladiator Dance Sounds Vol.2<br/>
				<img src="http://www.kreativsounds.com/img/presets/culture-electronic-gladiator-dance-sounds-v2-small.png" alt="CE Gladiator Dance Sounds Volume 2" class="image" width="100" height="95"/>
			</a>
			<br/>
			210 fantastic dance sounds for Tone2 Gladiator
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="CE002">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="3L6WBWYGUH5EN">
				<a href="#add2cart" onClick="javascript:document['CE002'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds vst-presets vanguard patch culture-electronic center ">
			<a href="http://www.kreativsounds.com/ce-vanguard-trance-world-sounds" title="CE Vanguard Trance Word Sounds">CE Vanguard Trance World Sounds<br/>
				<img src="http://www.kreativsounds.com/img/presets/culture-electronic-vanguard-trance-world-sounds-small.png" alt="CE Vanguard Trance Word Sounds" class="image" width="100" height="95"/>
			</a>
			<br/>
			158 exciting dance sounds for reFX Vanguard
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="CE003">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="5WB7AP6Q2ADSC">
				<a href="#add2cart" onClick="javascript:document['CE003'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>
	
		<li class="sounds audio-loops dr-octorex 16bit acid rex wav aiff kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-loops-ambient-textures" title="LOOPS Ambient Textures">LOOPS Ambient Textures<br/>
				<img src="http://www.kreativsounds.com/img/audio-loops/loops-ambient-textures-small.png" alt="LOOPS Ambient Textures" class="image" width="100" height="95"/>
			</a>
			<br/>
			48 original ambient, atmosphere and textures loops
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS301">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="QC9H8NR3RT3Q4">
				<a href="#add2cart" onClick="javascript:document['KS301'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds audio-loops dr-octorex 16bit acid rex wav aiff kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-loops-arp-chords" title="LOOPS Arp Chords">LOOPS Arp Chords<br/>
				<img src="http://www.kreativsounds.com/img/audio-loops/loops-arp-chords-small.png" alt="LOOPS Arp Chords" class="image" width="100" height="95"/>
			</a>
			<br/>
			54 original arpeggios, chords and phrases loops
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS302">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="C3FUF8VEUECN6">
				<a href="#add2cart" onClick="javascript:document['KS302'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds audio-loops dr-octorex 16bit acid rex wav aiff kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-loops-bass-drums" title="LOOPS Bass & Drums">LOOPS Bass & Drums<br/>
				<img src="http://www.kreativsounds.com/img/audio-loops/loops-bass-drums-small.png" alt="LOOPS Bass & Drums" class="image" width="100" height="95"/>
			</a>
			<br/>
			82 original synthesizer bass lines and drums loops
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS303">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="E5WHP6532YLT4">
				<a href="#add2cart" onClick="javascript:document['KS303'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>
		
		<li class="sounds audio-loops 16bit acid wav aiff kreativsound free center ">
			<a href="http://www.kreativsounds.com/free-sounds/" title="Read more about LOOPS Original Free">LOOPS Original Free<br/>
				<img src="http://www.kreativsounds.com/img/audio-loops/original-free-loops-small.png" alt="Read more about Original LOOPS Free" class="image" width="100" height="95" /></a>
			</a>
			<br/>
			50 free original loops in WAV & AIFF format
			<div class="addfree">
				<a href="http://www.kreativsounds.com/free-sounds/" title="LOOPS Original Free"> Download </a>
			</div>
		</li>
		
		<li class="sounds audio-loops dr-octorex 16bit acid rex wav aiff kreativsound free center ">
			<a href="http://www.kreativsounds.com/free-sounds/" title="LOOPS Salad Free">LOOPS Salad Free<br/>
				<img src="http://www.kreativsounds.com/img/audio-loops/ks-loops-salad-lite-small.png" alt="Download LOOPS Salad Free" class="image" width="100" height="95"/>
			</a>
			<br/>
			30 free premium loops in WAV, AIFF & REX format
			<div class="addfree">
				<a href="http://www.kreativsounds.com/free-sounds/" title="LOOPS Salad Free"> Download </a>
			</div>
		</li>		

		<li class="sounds audio-loops dr-octorex 16bit acid rex wav aiff sequencek center ">
			<a href="http://www.kreativsounds.com/ks-loops-sequencek-drums" title="LOOPS SequenceK Drums">LOOPS SequenceK Drums<br/>
				<img src="http://www.kreativsounds.com/img/audio-loops/loops-sequencek-drums-small.png" alt="LOOPS SequenceK Drums" class="image" width="100" height="95"/>
			</a>
			<br/>
			70 re-sampled, granulated and sfx-ed audio drum loops
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS307">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="Q6WMP68TUZ93Y">
				<a href="#add2cart" onClick="javascript:document['KS307'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds audio-loops dr-octorex 24bit acid rex wav aiff shortcircuit center ">
			<a href="http://www.kreativsounds.com/ks-loops-shortcircuit-drums" title="LOOPS ShortCircuit Drums">LOOPS ShortCircuit Drums<br/>
				<img src="http://www.kreativsounds.com/img/audio-loops/loops-shortcircuit-drums-small.png" alt="LOOPS ShortCircuit Drums" class="image" width="100" height="95"/>
			</a>		
			<br/>
			48 sci-fi, twisted and unique audio drum loops
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS308">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="PR4CFSE7KSYN6">
				<a href="#add2cart" onClick="javascript:document['KS308'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds audio-loops dr-octorex 16bit acid rex wav aiff kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-loops-sequential-riffs" title="LOOPS Sequential Riffs">LOOPS Sequential Riffs<br/>
				<img src="http://www.kreativsounds.com/img/audio-loops/loops-sequential-riffs-small.png" alt="LOOPS Sequential Riffs" class="image" width="100" height="95"/>
			</a>
			<br/>
			66 synthesizer sequences and trance lead riffs
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS304">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="234D7DX9MBWSE">
				<a href="#add2cart" onClick="javascript:document['KS304'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds audio-loops dr-octorex 16bit acid rex wav aiff kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-loops-sound-fx" title="LOOPS Sound FX">LOOPS Sound FX<br/>
				<img src="http://www.kreativsounds.com/img/audio-loops/loops-sound-fx-small.png" alt="LOOPS Sound FX" class="image" width="100" height="95"/>
			</a>
			<br/>
			24 sound effects and sfx manipulated audio loops
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS306">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="KTWET6E4KQ8T4">
				<a href="#add2cart" onClick="javascript:document['KS306'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds audio-loops dr-octorex 16bit acid rex wav aiff kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-loops-tb-303-bass-line" title="LOOPS TB-303 Bass Line">LOOPS TB-303 Bass Line<br/>
				<img src="http://www.kreativsounds.com/img/audio-loops/loops-tb-303-bass-line-small.png" alt="LOOPS TB-303 Bass Line" class="image" width="100" height="95"/>
			</a>
			<br/>
			52 TB-303 acid and bass lines audio loops
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS309">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="62FWAGWCU3TKA">
				<a href="#add2cart" onClick="javascript:document['KS309'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds audio-loops dr-octorex 16bit acid rex wav aiff kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-loops-twisted-vintage" title="LOOPS Twisted Vintage">LOOPS Twisted Vintage<br/>
				<img src="http://www.kreativsounds.com/img/audio-loops/loops-twisted-vintage-small.png" alt="LOOPS Twisted Vintage" class="image" width="100" height="95"/>
			</a>
			<br/>			
			46 twisted and vintage synthesizer audio loops
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS305">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="GK4TN677PDV8L">
				<a href="#add2cart" onClick="javascript:document['KS305'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>
		
		<li class="sounds reason-refills combinator patch kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-pro-combinators-v1" title="PRO Combinators Volume 1">PRO Combinators Volume 1<br/>
				<img src="http://www.kreativsounds.com/img/reason-refills/pro-combinators-v1-reason-refill-small.png" alt="PRO Combinators Volume 1"  class="image" width="100" height="95"/>
			</a>
			<br/>
			72 unique instruments for Reason Combinator Module
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS403">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="WZHB3DP2CR4S4">
				<a href="#add2cart" onClick="javascript:document['KS403'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds reason-refills combinator patch kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-pro-combinators-v2" title="PRO Combinators Volume 2">PRO Combinators Volume 2<br/>
				<img src="http://www.kreativsounds.com/img/reason-refills/pro-combinators-v2-reason-refill-small.png" alt="PRO Combinators Volume 2" class="image" width="100" height="95"/>
			</a>
			<br/>
			72 original instruments for Reason Combinator Module
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS406">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="PTE9ZM5YPKMFQ">
				<a href="#add2cart" onClick="javascript:document['KS406'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds reason-refills malstrom patch kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-pro-malstrom-sounds-v1" title="PRO Malstr&ouml;m Sounds Volume 1">PRO Malstr&ouml;m Sounds Volume 1<br/>
				<img src="http://www.kreativsounds.com/img/reason-refills/pro-malstrom-sounds-v1-reason-refill-small.png" alt="PRO Malstr&ouml;m Sounds Volume 1" class="image" width="100" height="95"/>
			</a>
			<br/>
			150 highly useful presets for Reason Malstr&ouml;m Synthesizer
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS402">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="MN7FKP28Q85Q4">
				<a href="#add2cart" onClick="javascript:document['KS402'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>
		
		<li class="sounds reason-refills malstrom patch kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-pro-malstrom-sounds-v2" title="PRO Malstr&ouml;m Sounds Volume 2">PRO Malstr&ouml;m Sounds Volume 2<br/>
				<img src="http://www.kreativsounds.com/img/reason-refills/pro-malstrom-sounds-v2-reason-refill-small.png" alt="PRO Malstr&ouml;m Sounds Volume 2" class="image" width="100" height="95"/>
			</a>
			<br/>
			150 outstanding presets for Reason Malstr&ouml;m Synthesizer
			<br/>
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS405">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="GFPGW8YBFJ5TJ">
				<a href="#add2cart" onClick="javascript:document['KS405'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>
		
		<li class="sounds reason-refills patch kreativsound free center">
			<a href="http://www.kreativsounds.com/free-sounds/" title="PRO Reason Sounds FREE">PRO Reason Sounds FREE<br/>
				<img src="http://www.kreativsounds.com/img/reason-refills/prowaves-lite-reason-refill-small.png" alt="Download PRO Reason Sounds Free" class="image" width="100" height="95" />
			</a>
			<br/>
			64 free presets for Reason Malstr&ouml;m, Subtractor and Combinator
			<div class="addfree">
					<a href="http://www.kreativsounds.com/free-sounds/" title="PRO Reason Sounds FREE"> Download </a>
			</div>
		</li>

		<li class="sounds reason-refills subtractor patch kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-pro-subtractor-sounds-v1" title="PRO SubTractor Sounds Volume 1">PRO SubTractor Sounds Volume 1<br/>
				<img src="http://www.kreativsounds.com/img/reason-refills/pro-subtractor-sounds-v1-reason-refill-small.png" alt="PRO SubTractor Sounds Volume 1" class="image" width="100" height="95"/>
			</a>
			<br/>
			150 addictive presets for Reason SubTractor Synthesizer
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS401">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="HXBBRZQ55CH5W">
				<a href="#add2cart" onClick="javascript:document['KS401'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds reason-refills subtractor patch kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-pro-subtractor-sounds-v2" title="PRO SubTractor Sounds Volume 2">PRO SubTractor Sounds Volume 2<br/>
				<img src="http://www.kreativsounds.com/img/reason-refills/pro-subtractor-sounds-v2-reason-refill-small.png" alt="PRO SubTractor Sounds Volume 2" class="image" width="100" height="95"/>
			</a>
			<br/>
			150 remarkable presets for Reason SubTractor Synthesizer
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS404">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="L5KVPVDDWUG52">
				<a href="#add2cart" onClick="javascript:document['KS404'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds reason-refills thor patch sequencek center ">
			<a href="http://www.kreativsounds.com/ks-sequencek-thor-sounds" title="PRO SequenceK THOR Sounds">PRO SequenceK THOR Sounds<br/>
				<img src="http://www.kreativsounds.com/img/reason-refills/pro-sequencek-thor-sounds-small.png" alt="PRO SequenceK THOR Sounds" class="image" width="100" height="95"/>
			</a>
			<br/>
			65 really playable presets for Reason THOR Synthesizer
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS407">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="TV4C9ZJPMZUX4">
				<a href="#add2cart" onClick="javascript:document['KS407'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>
		
		<li class="sounds multisamples 24bit wav kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-raw-bass" title="RAW Bass">RAW Bass<br/>
				<img src="http://www.kreativsounds.com/img/multiformat/raw-bass-small.png" alt="RAW Bass" class="image" width="100" height="95"/>
			</a>
			<br/>
			96 synthesized and digital bass multisamples
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS503">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="UZYBJ924MXAXE">
				<a href="#add2cart" onClick="javascript:document['KS503'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds multisamples redrum scream4 24bit wav kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-raw-drums" title="RAW Drums">RAW Drums<br/>
				<img src="http://www.kreativsounds.com/img/multiformat/raw-drums-small.png" alt="RAW Drums" class="image" width="100" height="95"/>
			</a>
			<br/>
			160 drums shots, 16 ReDrum kits, 8 Scream4
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS502">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="R2HCX5XMXLVXS">
				<a href="#add2cart" onClick="javascript:document['KS502'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds multisamples nnxt nn19 sfz kontakt combinator 24bit wav kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-raw-textures" title="RAW Textures">RAW Textures<br/>
				<img src="http://www.kreativsounds.com/img/multiformat/RAW-Textures-thumb.png" alt="RAW Textures" class="image" width="100" height="95"/>
			</a>
			<br/>
			32+ unique dark & light ambient textures
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS504">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="SM8LNHWMNV59A">
				<a href="#add2cart" onClick="javascript:document['KS504'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>
		
		<li class="sounds multisamples nnxt nn19 combinator 24bit wav kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-raw-transid-station" title="RAW TranSID">RAW TranSID<br/>
				<img src="http://www.kreativsounds.com/img/multiformat/raw-transid64-small.png" alt="RAW TranSID" class="image" width="100" height="95"/>
			</a>
			<br/>
			68 Lo-Fi, 8Bit and SID sounding instruments
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS501">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="AKC358YHJGZYN">
				<a href="#add2cart" onClick="javascript:document['KS501'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds audio-loops 16bit acid wav aiff sinevibes free center ">
			<a href="http://www.kreativsounds.com/sinevibes-ambisphere" title="Sinevibes Ambisphere">Sinevibes Ambisphere<br/>
				<img src="http://www.kreativsounds.com/img/audio-loops/sinevibes-ambisphere-small.png" alt="Sinevibes Ambisphere" class="image" width="100" height="95"/>
			</a>
			<br/>
			100 fresh ambient and atmospheric loops
			<div class="addfree">
				<a href="http://www.kreativsounds.com/free-sounds/" title="Sinevibes Beats"> Download </a>
			</div>
		</li>

		<li class="sounds audio-loops 16bit acid wav aiff sinevibes free center ">
			<a href="http://www.kreativsounds.com/sinevibes-beats" title="Sinevibes Beats">Sinevibes Beats<br/>
				<img src="http://www.kreativsounds.com/img/audio-loops/sinevibes-beats-small.png" alt="Sinevibes Beats" class="image" width="100" height="95"/>
			</a>
			<br/>
			100 fresh beats and rhythmic effect sounds
			<div class="addfree">
				<a href="http://www.kreativsounds.com/free-sounds/" title="Sinevibes Beats"> Download </a>
			</div>
		</li>

		<li class="sounds multisamples 16bit acid wav aiff sinevibes free center ">
			<a href="http://www.kreativsounds.com/sinevibes-constructor" title="Sinevibes Constructor">Sinevibes Constructor<br/>
				<img src="http://www.kreativsounds.com/img/audio-loops/sinevibes-constructor-small.png" alt="Sinevibes Constructor" class="image" width="100" height="95"/>
			</a>
			<br/>
			180 fresh drum shots and percussion sounds
			<div class="addfree">
				<a href="http://www.kreativsounds.com/free-sounds/" title="Sinevibes Constructor"> Download </a>
			</div>
		</li>

		<li class="sounds audio-loops 16bit acid wav aiff sinevibes free center ">
			<a href="http://www.kreativsounds.com/sinevibes-motions" title="Sinevibes Motions">Sinevibes Motions<br/>
				<img src="http://www.kreativsounds.com/img/audio-loops/sinevibes-motions-small.png" alt="Sinevibes Motions" class="image" width="100" height="95"/>
			</a>
			<br/>
			100 fresh synthesizer bass loops
			<div class="addfree">
				<a href="http://www.kreativsounds.com/free-sounds/" title="Sinevibes Motions"> Download </a>
			</div>
		</li>

		<li class="sounds audio-loops 16bit acid wav aiff sinevibes free center ">
			<a href="http://www.kreativsounds.com/sinevibes-vocoder" title="Sinevibes Vocoder">Sinevibes Vocoder<br/>
				<img src="http://www.kreativsounds.com/img/audio-loops/sinevibes-vocoder-small.png" alt="Sinevibes Vocoder" class="image" width="100" height="95"/>
			</a>
			<br/>
			80 various fresh vocoded sounds
			<div class="addfree">
				<a href="http://www.kreativsounds.com/free-sounds/" title="Sinevibes Vocoder"> Download </a>
			</div>
		</li>
				
		<li class="sounds vst-presets bass-station kore patch abyss kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-syn-bass-station-sounds" title="SYN Bass Station Sounds">SYN Bass Station Sounds<br/>
				<img src="http://www.kreativsounds.com/img/presets/syn-bass-station-sounds-small.png" alt="SYN Bass Station Sounds" class="image" width="100" height="95"/>
			</a>
			<br/>
			36 monstrous basses for Novation Bass Station
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS207">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="CZSKCJQFKPHSQ">
				<a href="#add2cart" onClick="javascript:document['KS207'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds vst-presets imposcar kore patch abyss kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-syn-imposcar-sounds" title="SYN ImpOSCar Sounds">SYN ImpOSCar Sounds<br/>
				<img src="http://www.kreativsounds.com/img/presets/syn-imposcar-sounds-v1-small.png" alt="SYN ImpOSCar Sounds" class="image" width="100" height="95"/>
			</a>
			<br/>
			54 sublime sounds for GForce ImpOSCar
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS203">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="EJHZYRZLJ4Q9Y">
				<a href="#add2cart" onClick="javascript:document['KS203'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds vst-presets minimoog-v kore patch abyss kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-syn-minimoog-v-sounds" title="SYN Minimoog V Sounds">SYN Minimoog V Sounds<br/>
				<img src="http://www.kreativsounds.com/img/presets/syn-minimoog-v-sounds-small.png" alt="SYN Minimoog V Sounds" class="image" width="100" height="95"/>
			</a>
			<br/>
			64 warm sounds for Arturia Minimoog V
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS208">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="L9PZLAYYNZNYE">
				<a href="#add2cart" onClick="javascript:document['KS208'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds vst-presets one kore patch abyss kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-syn-one-sounds" title="SYN One Sounds">SYN One Sounds<br/>
				<img src="http://www.kreativsounds.com/img/presets/syn-one-sounds-v1-small.png" alt="SYN One Sounds" class="image" width="100" height="95"/>
			</a>
			<br/>
			64 fat analog sounds for FabFilter One
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS206">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="X24DY3YW5QR4N">
				<a href="#add2cart" onClick="javascript:document['KS206'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds vst-presets prophet-v kore patch abyss kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-syn-prophet-v-sounds" title="SYN Prophet V Sounds">SYN Prophet V Sounds<br/>
				<img src="http://www.kreativsounds.com/img/presets/syn-prophet-v-sounds-v1-small.png" alt="SYN Prophet V Sounds" class="image" width="100" height="95"/>
			</a>
			<br/>
			52 vintage wave sounds for Arturia Prophet V
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS204">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="EJ8SQMVCND77L">
				<a href="#add2cart" onClick="javascript:document['KS204'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>

		<li class="sounds vst-presets sylenth1 kore patch abyss kreativsound center ">
			<a href="http://www.kreativsounds.com/ks-syn-sylenth1-sounds" title="SYN Sylenth1 Sounds">SYN Sylenth1 Sounds<br/>
				<img src="http://www.kreativsounds.com/img/presets/syn-sylenth1-sounds-v1-small.png" alt="SYN Sylenth1 Sounds" class="image" width="100" height="95"/>
			</a>
			<br/>
			22 new electro sounds for LennarDigital Sylenth1
			<div class="add2cart">
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" name="KS205">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="2YVFMXWYMR5JC">
				<a href="#add2cart" onClick="javascript:document['KS205'].submit()" title="Add To Cart with PayPal"><img src="http://www.kreativsounds.com/img/nav/cart.png" width="24" height="12" alt="Add To Cart with PayPal"/> Add to cart +</a>
				</form>
			</div>
		</li>		

    </ul>
	
</div>

<?php get_footer(); ?>