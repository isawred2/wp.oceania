<?php
/*
Template Name: Popular
*/
?>
<?php get_header(); ?>

<div id="page">

	<h1><?php the_title(); ?></h1>

	<div class="page_column">

		<h2>Most popular articles</h2>

		<?php if (function_exists('get_most_viewed')): ?>

			<ol class="page_column_list">

			<?php get_most_viewed_category(1,'post', 100); ?>

			</ol>

		<?php endif; ?>

	</div>

	<div class="page_column">

		<h2>Most popular themes</h2>

		<?php if (function_exists('get_most_viewed_category')): ?>

			<ol class="page_column_list">

			<?php get_most_viewed_category(3, 'post', 100); ?>

			</ol>

		<?php endif; ?>

	</div>

	<div class="page_column">

		<h2>Most popular freebies</h2>

		<?php if (function_exists('get_most_viewed_category')): ?>

			<ol class="page_column_list">

			<?php get_most_viewed_category(21, 'post', 100); ?>

			</ol>

		<?php endif; ?>

	</div>

</div>

<?php get_footer(); ?>