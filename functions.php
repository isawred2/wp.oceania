<?php

//Enable Custom Menus
if ( function_exists( 'add_theme_support' ) ) { 
	add_theme_support( 'menus' ); 
}

if ( function_exists( 'register_nav_menu' ) ) { 
	register_nav_menu( 'primary', 'Primary Menu' );
}

//Enable Custom Background
if ( function_exists( 'add_theme_support' ) ) { 
	add_theme_support( 'custom-background');
}

//Enable Post Thumbnails
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'archive-thumbnail', 160, 100, true );
	set_post_thumbnail_size( 160, 100);
}

//Enable Feed Links
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'automatic-feed-links');
}

//Enable Register Sidebar
if ( function_exists('register_sidebar') ) {
	register_sidebar(array('name' => 'Right Sidebar','before_widget' => '','after_widget' => '','before_title' => '<h3>','after_title' => '</h3>'));
	register_sidebar(array('name' => 'Footer Left','before_widget' => '','after_widget' => '','before_title' => '<h3>','after_title' => '</h3>'));
	register_sidebar(array('name' => 'Footer Center','before_widget' => '','after_widget' => '','before_title' => '<h3>','after_title' => '</h3>'));
	register_sidebar(array('name' => 'Footer Right','before_widget' => '','after_widget' => '','before_title' => '<h3>','after_title' => '</h3>'));
}

//Enable Custom Post Type
/*if ( function_exists('register_post_type') ) {
	register_post_type('product', array( 'label' => 'Products','description' => 'All Products','public' => true,'show_ui' => true,'show_in_menu' => true,'capability_type' => 'post','hierarchical' => false,'rewrite' => array('slug' => ''),'query_var' => true,'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes',),'taxonomies' => array('category','post_tag',),'labels' => array ( 'name' => 'Products', 'singular_name' => 'Product', 'menu_name' => 'Products', 'add_new' => 'Add Product', 'add_new_item' => 'Add New Product', 'edit' => 'Edit', 'edit_item' => 'Edit Product', 'new_item' => 'New Product', 'view' => 'View Product', 'view_item' => 'View Product', 'search_items' => 'Search Products', 'not_found' => 'No Products Found', 'not_found_in_trash' => 'No Products Found in Trash', 'parent' => 'Parent Product',),));
}*/

// Image resize in content
// To Use: [img w=600 h=200 class=alignleft alt=My Photo]http://mysite.com/photo.jpg[/img]
function timmyimg($atts, $content = null) {
	extract(shortcode_atts(array(
		'w' => '600',
		'h' => '250',
		'zc' => '0',
		'a' => 'c',
		'q' => '100',
		'class' => 'alignnone',
		'alt' => ''
	), $atts));
	return "<img src='". get_bloginfo('template_directory') . "/timthumb.php?src=".$content."&w=".$w."&h=".$h."&zc=".$zc."&q=".$q."&a=".$a."' alt='".$alt."' class='wp-post-img ".$class."' />";
}
add_shortcode('img', 'timmyimg');

function my_script_enqueue() {
   wp_deregister_script('jquery');
   wp_register_script('jquery', "http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js", false, NULL, true);
   wp_enqueue_script('jquery');
   wp_enqueue_script('lazyload', get_bloginfo('template_url') . '/js/jquery.lazyload.min.js', array('jquery'), NULL, true);
}
if (!is_admin()) add_action('init', 'my_script_enqueue');

function my_css_enqueue() {
   wp_dequeue_style('wp-postratings');
   wp_dequeue_style('wp-postviews');
   wp_dequeue_style('cptchStylesheet');
   wp_dequeue_style('cntctfrmStylesheet');
   wp_dequeue_style('mymail-form-css');
 }
if (!is_admin()) add_action('wp_enqueue_scripts', 'my_css_enqueue');

function remove_comments_rss( $for_comments ) {
    return;
}
add_filter('post_comments_feed_link','remove_comments_rss');

remove_action( 'wp_head', 'wp_generator' );

remove_action('wp_head', 'wlwmanifest_link');

remove_action('wp_head', 'rsd_link');

//allow php in sidebar widget
function php_execute($html){
	if(strpos($html,"<"."?php")!==false){
		ob_start();
		eval("?".">".$html);
		$html=ob_get_contents();
		ob_end_clean();
	}
	return $html;
}
add_filter('widget_text','php_execute',100);


function my_login_head() {
	echo "
	<style>
	body.login #login h1 a {
		background: url('".get_bloginfo('template_url')."/img/logo.png') no-repeat scroll center top transparent;
		height: 80px;
		width: 320px;
	}
	</style>
	";
}
add_action("login_head", "my_login_head");

//change login form URL
function my_login_logo_url() {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

//change login form info
function my_login_logo_url_title() {
    return get_bloginfo( 'name' );
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );
?>